<?php
/**
 * This file belongs to the YITH PNFW Purchase Note for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( 'plugin_options_badge_section' === $class ) {
	?>
	<fieldset class="form-field">
	<legend class="form-field__legend"><?php echo esc_html( $label ); ?></legend>
	<ul class="wc-radios">
		<?php foreach ( $options as $opt_value => $label ) : ?>
		<li class="<?php echo isset( $class ) ? esc_attr( $class ) : ''; ?>__radio">
			<label><input class="<?php echo isset( $class ) ? esc_attr( $class ) : ''; ?>__input" type="radio" name="<?php echo esc_attr( $id ); ?>" id="<?php echo esc_attr( $id . '__' . $label['value'] ); ?>" value="<?php echo esc_attr( $label['value'] ); ?>" <?php echo $label['value'] === $value ? 'checked' : ''; ?>><?php echo esc_html( $label['text'] ); ?></label>
		</li>
		<?php endforeach; ?>
	</ul>
	</fieldset>
<?php } else {
	?>
	<fieldset class="form-field">
	<legend class="form-field__legend"><?php echo esc_html( $label ); ?></legend>
	<ul class="wc-radios">
		<?php foreach ( $options as $opt_value => $label ) : ?>
		<li class="<?php echo isset( $class ) ? esc_attr( $class ) : ''; ?>__radio">
			<label><input class="<?php echo isset( $class ) ? esc_attr( $class ) : ''; ?>__input" type="radio" name="<?php echo esc_attr( $id ); ?>" id="<?php echo esc_attr( $id . '__' . $opt_value ); ?>" value="<?php echo esc_attr( $opt_value ); ?>"
			<?php
			if ( isset( $value ) && ! empty( $value ) ) {
				echo $opt_value === $value ? 'checked' : '';
			} else {
				echo $opt_value === $default ? 'checked' : '';
			}
			?>
			>
			<?php echo esc_html( $label ); ?>
			</label>
		</li>
		<?php endforeach; ?>
	</ul>
	</fieldset>
<?php } ?>
