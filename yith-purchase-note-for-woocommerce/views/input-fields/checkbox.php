<?php
/**
 * This file belongs to the YITH PNFW Purchase Note for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package YITH PNFW Purchase Note for WooCommerce
 */

?>
<p class="form-field">
	<label class="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>__label switch" for="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"><?php echo isset( $label ) ? esc_html( $label ) : ''; ?>
	<input class="<?php echo esc_attr( $id ); ?>__input" type="checkbox" name="<?php echo esc_attr( $id ); ?>" id="<?php echo esc_attr( $id ); ?>" value="<?php echo isset( $value ) && ! empty( $value ) ? esc_attr( $value ) : esc_attr( $default ); ?>" 
	<?php
	if ( isset( $value ) && ! empty( $value ) ) {
		echo 'yes' === $value ? 'checked' : '';
	} else {
		echo 'yes' === $default ? 'checked' : '';
	}
	?>
	>
	<span class="slider round"></span>
	</label>
</p>
