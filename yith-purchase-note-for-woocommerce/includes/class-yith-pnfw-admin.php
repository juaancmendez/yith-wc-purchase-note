<?php
/**
 * This file belongs to the YITH PNFW Purchase Note For WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PNFW_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PNFW_Admin' ) ) {
	/**
	 * YITH_PNFW_Admin
	 */
	class YITH_PNFW_Admin {
		/**
		 * Main Instance
		 *
		 * @var YITH_PNFW_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		private $purchase_note_fields = array();

		public $pnfw_settings = array();
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PNFW_Admin Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PNFW_Admin constructor.
		 */
		private function __construct() {

			$this->set_fields();
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'create_purchase_note_tab' ) );
			add_filter( 'woocommerce_product_data_panels', array( $this, 'purchase_note_panel' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_fields' ) );
			add_action( 'admin_menu', array( $this, 'pnfw_create_options_menu' ) );
			add_action( 'admin_init', array( $this, 'pnfw_register_settings' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ), 99 );

		}
		/**
		 * Enqueue_scripts.
		 *
		 * @return void
		 */
		public function enqueue_scripts( ) {
			wp_register_script( 'yith-pnfw-colorpicker-js', YITH_PNFW_DIR_ASSETS_JS_URL . '/pnfw-colorpicker.js', array( 'jquery', 'wp-color-picker' ), YITH_PNFW_VERSION, true );
			wp_register_script( 'yith-pnfw-metabox-main-js', YITH_PNFW_DIR_ASSETS_JS_URL . '/pnfw-metabox.js', array( 'jquery' ), YITH_PNFW_VERSION, true );
			wp_register_style( 'yith-pnfw-plugin-options-css', YITH_PNFW_DIR_ASSETS_CSS_URL . '/pnfw-plugin-options.css', array(), YITH_PNFW_VERSION );
			wp_register_style( 'yith-pnfw-product-options-css', YITH_PNFW_DIR_ASSETS_CSS_URL . '/pnfw-product-options.css', array(), YITH_PNFW_VERSION );
			if ( is_admin() ) { //ver cuales serian los indicados
				wp_enqueue_style( 'wp-color-picker' );
				wp_enqueue_script( 'yith-pnfw-colorpicker-js' );
				wp_enqueue_script( 'yith-pnfw-metabox-main-js' );
				wp_enqueue_style( 'yith-pnfw-product-options-css' );
				wp_enqueue_style( 'yith-pnfw-plugin-options-css' );
			}
		}

		/**
		 * Add the new tab to the $tabs array
		 *
		 * @param array $tabs .
		 * @since   1.0.0
		 */
		public function create_purchase_note_tab( $tabs ) {
			$tabs['purchase_note'] = array(
				'label'    => __( 'Purchase Note', 'pnfw' ), // The name of your panel
				'target'   => 'purchase_note_panel', // Will be used to create an anchor link so needs to be unique
				'class'    => array( 'purchase_note_tab', 'show_if_simple', 'show_if_variable' ), // Class for your panel tab - helps hide/show depending on product type
				'priority' => 80, // Where your panel will appear. By default, 70 is last item
			);
			return $tabs;
		}
		/**
		 * Set_fields
		 *
		 * @return void
		 */
		public function set_fields() {
			$this->purchase_note_fields = array(
				'enable'         => array(
					'type'    => 'checkbox',
					'id'      => '_yith_pnfw_enable',
					'label'   => __( 'Enable Product Note', 'yith-purchase-note-for-woocommerce' ),
					'value'   => '',
					'default' => 'no',
				),
				'label'          => array(
					'type'    => 'text',
					'id'      => '_yith_pnfw_label',
					'label'   => __( 'Note label', 'yith-purchase-note-for-woocommerce' ),
					'value'   => '',
					'default' => 'Note',
				),
				'description'    => array(
					'type'    => 'textarea',
					'id'      => '_yith_pnfw_description',
					'label'   => __( 'Note description', 'yith-purchase-note-for-woocommerce' ),
					'value'   => '',
					'default' => '',
				),
				'field_type'     => array(
					'type'    => 'radio',
					'id'      => '_yith_pnfw_field_type',
					'label'   => __( 'Field type', 'yith-purchase-note-for-woocommerce' ),
					'options' => array(
						'text'     => __( 'Text', 'yith-purchase-note-for-woocommerce' ),
						'textarea' => __( 'Textarea', 'yith-purchase-note-for-woocommerce' ),
					),
					'value'   => '',
					'default' => 'text',
				),
				'price_settings' => array(
					'type'    => 'radio',
					'id'      => '_yith_pnfw_price_settings',
					'label'   => __( 'Price settings', 'yith-purchase-note-for-woocommerce' ),
					'options' => array(
						'free'           => __( 'Free', 'yith-purchase-note-for-woocommerce' ),
						'fixed_price'    => __( 'Fixed price', 'yith-purchase-note-for-woocommerce' ),
						'price_per_char' => __( 'Price per character', 'yith-purchase-note-for-woocommerce' ),
					),
					'value'   => '',
					'default' => 'free',
				),
				'price'          => array(
					'type'  => 'number',
					'id'    => '_yith_pnfw_price',
					'label' => __( 'Price', 'yith-purchase-note-for-woocommerce' ),
					'value' => '',
				),
				'free_chars'     => array(
					'type'    => 'number',
					'id'      => '_yith_pnfw_free_chars',
					'label'   => __( 'Free characters', 'yith-purchase-note-for-woocommerce' ),
					'value'   => '',
					'default' => 0,
				),
				'show_badge'     => array(
					'type'  => 'checkbox',
					'id'    => '_yith_pnfw_show_badge',
					'label' => __( 'Show badge', 'yith-purchase-note-for-woocommerce' ),
					'value' => '',
				),
				'badge_text'     => array(
					'type'    => 'text',
					'id'      => '_yith_pnfw_badge_text',
					'label'   => __( 'Badge text', 'yith-purchase-note-for-woocommerce' ),
					'value'   => '',
					'default' => '',
				),
				'badge_bc'       => array(
					'type'    => 'text',
					'id'      => '_yith_pnfw_badge_bc',
					'label'   => __( 'Badge background color', 'yith-purchase-note-for-woocommerce' ),
					'value'   => '',
					'default' => '#007694',
				),
				'badge_tc'       => array(
					'type'    => 'text',
					'id'      => '_yith_pnfw_badge_tc',
					'label'   => __( 'Badge text color', 'yith-purchase-note-for-woocommerce' ),
					'value'   => '',
					'default' => '#ffffff',
				),
			);
		}
		/**
		 * Set_values_fields
		 *
		 * @param  mixed $campos
		 * @return void
		 */
		public function set_values_fields( array $campos ) { //phpcs:ignore
			global $post;
			$product       = wc_get_product( $post->ID );
			$values_fields = array();

			foreach ( $campos as $key => $field ) {
				$meta = $product->get_meta( $field['id'] );

				if ( null !== $meta ) {
					$field['value'] = $meta;
				}

				$values_fields[ $key ] = $field;
			}

			return $values_fields;
		}
		/**
		 * Print_fields
		 *
		 * @param  mixed $campos
		 * @return void
		 */
		public function print_fields( array $campos ) {
			foreach ( $campos as $field ) {
				yith_pnfw_get_view( '/input-fields' . '/' . $field['type'] . '.php', $field );
			}
		}
		/**
		 * Save the custom fields using CRUD method
		 * @param $post_id
		 * @since 1.0.0
		 */
		public function save_fields( $post_id ) {

			$product = wc_get_product( $post_id );

			// meta inputs purchase note settings.

			$yith_pnfw_enable = isset( $_POST['_yith_pnfw_enable'] ) ? 'yes' : 'no';
			$product->update_meta_data( '_yith_pnfw_enable', sanitize_text_field( $yith_pnfw_enable ) );

			$yith_pnfw_label = isset( $_POST['_yith_pnfw_label'] ) && ! empty( $_POST['_yith_pnfw_label'] ) ? $_POST['_yith_pnfw_label'] : 'Note';
			$product->update_meta_data( '_yith_pnfw_label', sanitize_text_field( $yith_pnfw_label ) );

			$yith_pnfw_description = isset( $_POST['_yith_pnfw_description'] ) ? $_POST['_yith_pnfw_description'] : '';
			$product->update_meta_data( '_yith_pnfw_description', sanitize_text_field( $yith_pnfw_description ) );

			$yith_pnfw_field_type = isset( $_POST['_yith_pnfw_field_type'] ) && ! empty( $_POST['_yith_pnfw_field_type'] ) ? $_POST['_yith_pnfw_field_type'] : 'text';
			$product->update_meta_data( '_yith_pnfw_field_type', sanitize_text_field( $yith_pnfw_field_type ) );

			$yith_pnfw_price_settings = isset( $_POST['_yith_pnfw_price_settings'] ) && ! empty( $_POST['_yith_pnfw_price_settings'] ) ? $_POST['_yith_pnfw_price_settings'] : 'free';
			$product->update_meta_data( '_yith_pnfw_price_settings', sanitize_text_field( $yith_pnfw_price_settings ) );

			$yith_pnfw_price = isset( $_POST['_yith_pnfw_price'] ) && ! empty( $_POST['_yith_pnfw_price'] ) ? $_POST['_yith_pnfw_price'] : '';
			$product->update_meta_data( '_yith_pnfw_price', sanitize_text_field( $yith_pnfw_price ) );

			$yith_pnfw_free_chars = isset( $_POST['_yith_pnfw_free_chars'] ) && ! empty( $_POST['_yith_pnfw_free_chars'] ) ? $_POST['_yith_pnfw_free_chars'] : 0;
			$product->update_meta_data( '_yith_pnfw_free_chars', sanitize_text_field( $yith_pnfw_free_chars ) );

			$yith_pnfw_show_badge = isset( $_POST['_yith_pnfw_show_badge'] ) ? 'yes' : 'no';
			$product->update_meta_data( '_yith_pnfw_show_badge', sanitize_text_field( $yith_pnfw_show_badge ) );

			$yith_pnfw_badge_text = isset( $_POST['_yith_pnfw_badge_text'] ) && ! empty( $_POST['_yith_pnfw_badge_text'] ) ? $_POST['_yith_pnfw_badge_text'] : '';
			$product->update_meta_data( '_yith_pnfw_badge_text', sanitize_text_field( $yith_pnfw_badge_text ) );

			$yith_pnfw_badge_bc = isset( $_POST['_yith_pnfw_badge_bc'] ) && ! empty( $_POST['_yith_pnfw_badge_bc'] ) ? $_POST['_yith_pnfw_badge_bc'] : '#007694';
			$product->update_meta_data( '_yith_pnfw_badge_bc', sanitize_text_field( $yith_pnfw_badge_bc ) );

			$yith_pnfw_badge_tc = isset( $_POST['_yith_pnfw_badge_tc'] ) && ! empty( $_POST['_yith_pnfw_badge_tc'] ) ? $_POST['_yith_pnfw_badge_tc'] : '#ffffff';
			$product->update_meta_data( '_yith_pnfw_badge_tc', sanitize_text_field( $yith_pnfw_badge_tc ) );

			$product->save();

		}

		/**
		 * Purchase_note_panel
		 *
		 * @return void
		 */
		public function purchase_note_panel() {
			echo '<div id="purchase_note_panel" class="panel woocommerce_options_panel">';
			echo '<div class="options_group">';
			echo $this->print_fields( $this->set_values_fields( $this->purchase_note_fields ) ); //phpcs:ignore
			echo '</div>';
			echo '</div>';
		}

		/**
		 *  Create menu for general options
		 */
		public function pnfw_create_options_menu() {
			add_menu_page(
				esc_html__( 'YITH Purchase Note', 'yith-purchase-note-for-woocommerce' ),
				esc_html__( 'YITH Purchase Note', 'yith-purchase-note-for-woocommerce' ),
				'manage_options',
				'pnfw_options',
				array( $this, 'pnfw_custom_menu_page' ),
				'',
				50
			);
		}

		/**
		 * Callback custom menu page
		 *
		 * @return void
		 */
		public function pnfw_custom_menu_page() {
			yith_pnfw_get_view( '/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Add the fields in the shortcode attribute management page
		 */
		public function pnfw_register_settings() {
			$page_name = 'pnfw-options-page';

			$sections = array(
				array(
					'id'           => 'pnfw_padding_section',
					'title'        => esc_html__( 'Padding', 'yith-purchase-note-for-woocommerce' ),
					'section_name' => 'padding_options',
					'container'    => 'setting_options',
					'fields'       => array(
						array(
							'id'      => 'yith_pnfw_padding_top',
							'label'   => esc_html__( 'top', 'yith-purchase-note-for-woocommerce' ),
							'type'    => 'number',
							'name'    => 'yith_pnfw_padding_top',
							'default' => 20,
							'value'   => get_option(
								'yith_pnfw_padding_top',
								20
							),
						),
						array(
							'id'      => 'yith_pnfw_padding_right',
							'label'   => esc_html__( 'right', 'yith-purchase-note-for-woocommerce' ),
							'type'    => 'number',
							'name'    => 'yith_pnfw_padding_right',
							'default' => 25,
							'value'   => get_option(
								'yith_pnfw_padding_right',
								25
							),
						),
						array(
							'id'      => 'yith_pnfw_padding_bottom',
							'label'   => esc_html__( 'bottom', 'yith-purchase-note-for-woocommerce' ),
							'type'    => 'number',
							'name'    => 'yith_pnfw_padding_bottom',
							'default' => 25,
							'value'   => get_option(
								'yith_pnfw_padding_bottom',
								25
							),
						),
						array(
							'id'      => 'yith_pnfw_padding_left',
							'label'   => esc_html__( 'left', 'yith-purchase-note-for-woocommerce' ),
							'type'    => 'number',
							'name'    => 'yith_pnfw_padding_left',
							'default' => 25,
							'value'   => get_option(
								'yith_pnfw_padding_left',
								25
							),
						),
					),
				),
				array(
					'id'           => 'pnfw_border_section',
					'title'        => esc_html__( 'Border', 'yith-purchase-note-for-woocommerce' ),
					'section_name' => 'border_options',
					'container'    => 'setting_options',
					'fields'       => array(
						array(
							'id'      => 'yith_pnfw_border_width',
							'label'   => esc_html__( 'width', 'yith-purchase-note-for-woocommerce' ),
							'type'    => 'number',
							'name'    => 'yith_pnfw_border_width',
							'default' => 1,
							'value'   => get_option(
								'yith_pnfw_border_width',
								1
							),
						),
						array(
							'id'      => 'yith_pnfw_border_select',
							'label'   => esc_html__( 'style', 'yith-purchase-note-for-woocommerce' ),
							'type'    => 'select',
							'name'    => 'yith_pnfw_border_select',
							'default' => 'solid',
							'value'   => get_option(
								'yith_pnfw_border_select',
								'solid'
							),
							'options' => array(
								array(
									'value' => 'none',
									'text'  => __( 'None', 'yith-purchase-note-for-woocommerce' ),
									'id'    => 'yith-pnfw-border-style-none',
								),
								array(
									'value'   => 'solid',
									'text'    => __( 'Solid', 'yith-purchase-note-for-woocommerce' ),
									'id'      => 'yith-pnfw-border-style-solid',
									'default' => 1,
								),
								array(
									'value' => 'double',
									'text'  => __( 'Double', 'yith-purchase-note-for-woocommerce' ),
									'id'    => 'yith-pnfw-border-style-double',
								),
								array(
									'value' => 'dotted',
									'text'  => __( 'Dotted', 'yith-purchase-note-for-woocommerce' ),
									'id'    => 'yith-pnfw-border-style-dotted',
								),
							),
						),
						array(
							'id'      => 'yith_pnfw_border_color',
							'label'   => esc_html__( 'color', 'yith-purchase-note-for-woocommerce' ),
							'type'    => 'text',
							'name'    => 'yith_pnfw_border_color',
							'default' => '#d8d8d8',
							'value'   => get_option(
								'yith_pnfw_border_color',
								'#d8d8d8'
							),
						),
						array(
							'id'      => 'yith_pnfw_border_radius',
							'label'   => esc_html__( 'radius', 'yith-purchase-note-for-woocommerce' ),
							'type'    => 'number',
							'name'    => 'yith_pnfw_border_radius',
							'default' => 7,
							'value'   => get_option(
								'yith_pnfw_border_radius',
								7
							),
						),
					),
				),
				array(
					'id'           => 'pnfw_badge_section',
					'title'        => esc_html__( 'Badge Position', 'yith-purchase-note-for-woocommerce' ),
					'section_name' => 'padding_options',
					'container'    => 'setting-options',
					'fields'       => array(
						array(
							'id'      => 'yith_pnfw_badge_position_shop',
							'label'   => esc_html__( 'In Shop', 'yith-purchase-note-for-woocommerce' ),
							'class'   => 'plugin_options_badge_section',
							'type'    => 'radio',
							'name'    => 'yith_pnfw_badge_position_shop',
							'default' => 'top-right',
							'value'   => get_option(
								'yith_pnfw_badge_position_shop',
								'top-right'
							),
							'options' => array(
								array(
									'value' => 'top-right',
									'text'  => __( 'Top Right', 'yith-purchase-note-for-woocommerce' ),
									'id'    => 'yith_pnfw_badge_position_shop_tr',
								),
								array(
									'value' => 'top-left',
									'text'  => __( 'Top Left', 'yith-purchase-note-for-woocommerce' ),
									'id'    => 'yith_pnfw_badge_position_shop_tl',
								),
							),
						),
						array(
							'id'      => 'yith_pnfw_badge_position_product',
							'label'   => esc_html__( 'In Product', 'yith-purchase-note-for-woocommerce' ),
							'class'   => 'plugin_options_badge_section',
							'type'    => 'radio',
							'name'    => 'yith_pnfw_badge_position_product',
							'default' => 'top-right',
							'value'   => get_option(
								'yith_pnfw_badge_position_product',
								'top-right'
							),
							'options' => array(
								array(
									'value' => 'top-right',
									'text'  => __( 'Top Right', 'yith-purchase-note-for-woocommerce' ),
									'id'    => 'yith_pnfw_badge_position_product_tr',
								),
								array(
									'value' => 'top-left',
									'text'  => __( 'Top Left', 'yith-purchase-note-for-woocommerce' ),
									'id'    => 'yith_pnfw_badge_position_product_tl',
								),
							),
						),
					),
				),
			);

			foreach ( $sections as $section ) {
				add_settings_section(
					$section['id'],
					$section['title'],
					'',
					$page_name
				);

				foreach ( $section['fields'] as $field ) {

					if ( isset( $field['id'] ) ) {
						add_settings_field(
							$field['id'],
							'',
							array( $this, 'pnfw_print_settings' ),
							$page_name,
							$section['id'],
							$field,
						);
					}

					register_setting( $page_name, $field['name'] );
				}
			}
		}		
		/**
		 * Pnfw_print_settings
		 *
		 * @param  mixed $field_data
		 * @param  mixed $index
		 * @return void
		 */
		public function pnfw_print_settings( $field_data, $index = null ) {
			$default    = array(
				'label'           => '',
				'name'            => '',
				'id'              => '',
				'type'            => 'text',
				'container_class' => '',
				'container_id'    => '',
				'label_class'     => '',
				'input_class'     => '',
				'options'         => array(),
			);
			$field_data = array_merge( $default, $field_data );
			if ( array_key_exists( 'type', $field_data ) ) {
				echo '<div id="wpbody-content">';
				yith_pnfw_get_view( '/input-fields' . '/' . $field_data['type'] . '.php', $field_data, $index ); //phpcs:ignore
				echo '</div>';
			}
		}

	}
}
