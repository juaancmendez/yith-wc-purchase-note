<?php
/**
 * 
 */

?>

<div class="wrap settings-container">
    <h1><?php esc_html_e( 'Note Box Data', 'yith-purchase-note-for-woocommerce' ); ?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'pnfw-options-page' );
		    do_settings_sections( 'pnfw-options-page' );
            submit_button();
        ?>

    </form>
</div>