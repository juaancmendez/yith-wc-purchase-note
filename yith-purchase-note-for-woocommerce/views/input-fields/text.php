<?php
/**
 * This file belongs to the YITH PNFW Purchase Note for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package YITH PNFW Purchase Note for WooCommerce
 */

?>

<?php
if ( isset( $class ) && 'container-note' === $class ) {
    ?>
    <div class="<?php echo esc_attr( $class ); ?>">
    <p class="title-note"><?php echo esc_html( $title_note ); ?></p>
    <p class="description-note"><?php echo esc_html( $title_description ); ?></p>
    <p class="content-note"><input type="text" id="_yith_pnfw_content_note" name="_yith_pnfw_content_note" value="<?php echo esc_attr( $value ); ?>" placeholder="Your text note.." data-id="<?php echo esc_attr( $id_post ); ?>" required></p>
    <?php
    if ( 'free' === $price_type ) {
        ?>
		<p class="price-note">Free <?php echo esc_html( $currency_symbol ); ?></p>
        <?php
    } elseif ( 'fixed_price' === $price_type ) {
        ?>
		<p class="price-note">+ <?php echo esc_html( $price_note ) . ' ' . esc_html( $currency_symbol ); ?></p>
		<p class="price-free-chars <?php echo esc_attr( $display_none ); ?>">Free characters: <span class="ppc"><?php echo esc_html( $free_chars ); ?> </span> of <?php echo esc_html( $free_chars ); ?></p>
		<p class="price-note-final">Final price: <span class="ppp"><?php echo esc_html( $price_product ); ?></span> <?php echo esc_html( $currency_symbol ); ?></p>
        <?php
	} else {
        ?>
		<p class="price-note">+ <?php echo esc_html( $price_note ) . ' ' . esc_html( $currency_symbol ); ?> per character</p>
		<p class="price-free-chars <?php echo esc_attr( $display_none ); ?>">Free characters: <span class="ppc"><?php echo esc_html( $free_chars ); ?> </span> of <?php echo esc_html( $free_chars ); ?></p>
		<p class="price-note-final">Final price: <span class="ppp"><?php echo esc_html( $price_product ); ?></span> <?php echo esc_html( $currency_symbol ); ?></p>
	</div>
        <?php
    }
    ?>
<?php } else { ?>
    <p class="form-field <?php echo isset( $class ) ? esc_attr( $class ) : ''; ?>">
<label for="<?php echo esc_attr( $id ); ?>">
    <?php echo esc_html( $label ); ?>
</label>
<input type="text" id="<?php echo esc_attr( $id ); ?>"
   name="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
   value="<?php echo isset( $value ) && ! empty( $value ) ? esc_attr( $value ) : esc_attr( $default ); ?>"
   required
>
</p>
<?php } ?>
