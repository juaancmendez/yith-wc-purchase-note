jQuery(document).ready( function(){
    // condiciones al cargar la pagina
    if( jQuery("#_yith_pnfw_enable").is(':checked') ) {
        jQuery("label[for='_yith_pnfw_label']").show()
        jQuery("#_yith_pnfw_label").show()

        jQuery("label[for='_yith_pnfw_description']").show()
        jQuery("#_yith_pnfw_description").show()

        jQuery("fieldset[class='form-field']").show()

        jQuery("label[for='_yith_pnfw_field_type']").show()
        jQuery("#_yith_pnfw_field_type").show()

        jQuery("label[for='_yith_pnfw_price_settings']").show()
        jQuery("#_yith_pnfw_price_settings").show()

        if( jQuery("#_yith_pnfw_price_settings__fixed_price").is(':checked') || jQuery("#_yith_pnfw_price_settings__price_per_char").is(':checked') ) {
            jQuery("label[for='_yith_pnfw_price']").show()
            jQuery("#_yith_pnfw_price").show()

            jQuery("label[for='_yith_pnfw_free_chars']").show()
            jQuery("#_yith_pnfw_free_chars").show()
        } else {
            jQuery("label[for='_yith_pnfw_price']").hide()
            jQuery("#_yith_pnfw_price").hide()

            jQuery("label[for='_yith_pnfw_free_chars']").hide()
            jQuery("#_yith_pnfw_free_chars").hide()
        }

        jQuery("label[for='_yith_pnfw_show_badge']").show()
        jQuery("#_yith_pnfw_show_badge").show()

        if( jQuery("#_yith_pnfw_show_badge").is(':checked') ){
            jQuery("label[for='_yith_pnfw_badge_text']").show()
            jQuery("#_yith_pnfw_badge_text").show()

            jQuery("label[for='_yith_pnfw_badge_bc']").show()
            jQuery(".wp-picker-container").show()

            jQuery("label[for='_yith_pnfw_badge_tc']").show()
            //jQuery("#_yith_pnfw_badge_tc").show()
        } else {
            jQuery("label[for='_yith_pnfw_badge_text']").hide()
            jQuery("#_yith_pnfw_badge_text").hide()

            jQuery("label[for='_yith_pnfw_badge_bc']").hide()
            jQuery(".wp-picker-container").hide()

            jQuery("label[for='_yith_pnfw_badge_tc']").hide()
            //jQuery("#_yith_pnfw_badge_tc").hide()    
        }          
    } else {
        jQuery("label[for='_yith_pnfw_label']").hide()
        jQuery("#_yith_pnfw_label").hide()

        jQuery("label[for='_yith_pnfw_description']").hide()
        jQuery("#_yith_pnfw_description").hide()

        jQuery("fieldset[class='form-field']").hide()

        jQuery("label[for='_yith_pnfw_field_type']").hide()
        jQuery("#_yith_pnfw_field_type").hide()

        jQuery("label[for='_yith_pnfw_price_settings']").hide()
        jQuery("#_yith_pnfw_price_settings").hide()

        jQuery("label[for='_yith_pnfw_price']").hide()
        jQuery("#_yith_pnfw_price").hide()

        jQuery("label[for='_yith_pnfw_free_chars']").hide()
        jQuery("#_yith_pnfw_free_chars").hide()

        jQuery("label[for='_yith_pnfw_show_badge']").hide()
        jQuery("#_yith_pnfw_show_badge").hide()

        jQuery("label[for='_yith_pnfw_badge_text']").hide()
        jQuery("#_yith_pnfw_badge_text").hide()

        jQuery("label[for='_yith_pnfw_badge_bc']").hide()
        jQuery(".wp-picker-container").hide()

        jQuery("label[for='_yith_pnfw_badge_tc']").hide()
        //jQuery("#_yith_pnfw_badge_tc").hide()
    }
    // condiciones al haber un cambio en el checkbox principal
    jQuery("#_yith_pnfw_enable").on( 'change', function() {
        if( jQuery(this).is(':checked') ) {
            jQuery("label[for='_yith_pnfw_label']").show()
            jQuery("#_yith_pnfw_label").show()

            jQuery("label[for='_yith_pnfw_description']").show()
            jQuery("#_yith_pnfw_description").show()

            jQuery("fieldset[class='form-field']").show()

            jQuery("label[for='_yith_pnfw_field_type']").show()
            jQuery("#_yith_pnfw_field_type").show()

            jQuery("label[for='_yith_pnfw_price_settings']").show()
            jQuery("#_yith_pnfw_price_settings").show()

            if( jQuery("#_yith_pnfw_price_settings__fixed_price").is(':checked') || jQuery("#_yith_pnfw_price_settings__price_per_char").is(':checked') ) {
                jQuery("label[for='_yith_pnfw_price']").show()
                jQuery("#_yith_pnfw_price").show()
    
                jQuery("label[for='_yith_pnfw_free_chars']").show()
                jQuery("#_yith_pnfw_free_chars").show()
            } else {
                jQuery("label[for='_yith_pnfw_price']").hide()
                jQuery("#_yith_pnfw_price").hide()
    
                jQuery("label[for='_yith_pnfw_free_chars']").hide()
                jQuery("#_yith_pnfw_free_chars").hide()
            }

            jQuery("label[for='_yith_pnfw_show_badge']").show()
            jQuery("#_yith_pnfw_show_badge").show()

            if( jQuery("#_yith_pnfw_show_badge").is(':checked') ){
                jQuery("label[for='_yith_pnfw_badge_text']").show()
                jQuery("#_yith_pnfw_badge_text").show()

                jQuery("label[for='_yith_pnfw_badge_bc']").show()
                jQuery(".wp-picker-container").show()

                jQuery("label[for='_yith_pnfw_badge_tc']").show()
                //jQuery("#_yith_pnfw_badge_tc").show()
            }            
        } else {
            jQuery("label[for='_yith_pnfw_label']").hide()
            jQuery("#_yith_pnfw_label").hide()

            jQuery("label[for='_yith_pnfw_description']").hide()
            jQuery("#_yith_pnfw_description").hide()

            jQuery("fieldset[class='form-field']").hide()

            jQuery("label[for='_yith_pnfw_field_type']").hide()
            jQuery("#_yith_pnfw_field_type").hide()

            jQuery("label[for='_yith_pnfw_price_settings']").hide()
            jQuery("#_yith_pnfw_price_settings").hide()

            jQuery("label[for='_yith_pnfw_price']").hide()
            jQuery("#_yith_pnfw_price").hide()

            jQuery("label[for='_yith_pnfw_free_chars']").hide()
            jQuery("#_yith_pnfw_free_chars").hide()

            jQuery("label[for='_yith_pnfw_show_badge']").hide()
            jQuery("#_yith_pnfw_show_badge").hide()

            jQuery("label[for='_yith_pnfw_badge_text']").hide()
            jQuery("#_yith_pnfw_badge_text").hide()

            jQuery("label[for='_yith_pnfw_badge_bc']").hide()
            jQuery(".wp-picker-container").hide()

            jQuery("label[for='_yith_pnfw_badge_tc']").hide()
            //jQuery("#_yith_pnfw_badge_tc").hide()
        }
    });

    // condiciones si hay cambios en los checkbox del badge
    jQuery("#_yith_pnfw_show_badge").on( 'change', function() {
        if( jQuery(this).is(':checked') ) {
            jQuery("label[for='_yith_pnfw_badge_text']").show()
            jQuery("#_yith_pnfw_badge_text").show()

            jQuery("label[for='_yith_pnfw_badge_bc']").show()
            jQuery(".wp-picker-container").show()

            jQuery("label[for='_yith_pnfw_badge_tc']").show()
            //jQuery("#_yith_pnfw_badge_tc").show()
        } else {
            jQuery("label[for='_yith_pnfw_badge_text']").hide()
            jQuery("#_yith_pnfw_badge_text").hide()

            jQuery("label[for='_yith_pnfw_badge_bc']").hide()
            jQuery(".wp-picker-container").hide()

            jQuery("label[for='_yith_pnfw_badge_tc']").hide()
            //jQuery("#_yith_pnfw_badge_tc").hide()
        }
    });

    // condiciones si hay cambios en los checkbox price settings
    jQuery("#_yith_pnfw_price_settings__free").on( 'change', function() {
        if( jQuery(this).is(':checked') ) {
            jQuery("label[for='_yith_pnfw_price']").hide()
            jQuery("#_yith_pnfw_price").hide()

            jQuery("label[for='_yith_pnfw_free_chars']").hide()
            jQuery("#_yith_pnfw_free_chars").hide()    
        } else {
            jQuery("label[for='_yith_pnfw_price']").show()
            jQuery("#_yith_pnfw_price").show()

            jQuery("label[for='_yith_pnfw_free_chars']").show()
            jQuery("#_yith_pnfw_free_chars").show()     
        }   
    });
    jQuery("#_yith_pnfw_price_settings__fixed_price").on( 'change', function() {
        if( jQuery(this).is(':checked') ) {
            jQuery("label[for='_yith_pnfw_price']").show()
            jQuery("#_yith_pnfw_price").show()

            jQuery("label[for='_yith_pnfw_free_chars']").show()
            jQuery("#_yith_pnfw_free_chars").show()      
        }    
    });
    jQuery("#_yith_pnfw_price_settings__price_per_char").on( 'change', function() {
        if( jQuery(this).is(':checked') ) {
            jQuery("label[for='_yith_pnfw_price']").show()
            jQuery("#_yith_pnfw_price").show()

            jQuery("label[for='_yith_pnfw_free_chars']").show()
            jQuery("#_yith_pnfw_free_chars").show()      
        }    
    }); 

});