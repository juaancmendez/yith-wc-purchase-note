<?php
/**
 * This file belongs to the YITH PNFW Purchase Note for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

?>
<p class="form-field">
<label for="<?php echo esc_attr( $id ); ?>">
    <?php echo esc_html( $label ); ?>
</label>
<input type="number" id="<?php echo esc_attr( $id ); ?>"
       name="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
       min="<?php echo isset( $min ) ? esc_attr( $min ) : ''; ?>"
       max="<?php echo isset( $max ) ? esc_attr( $max ) : ''; ?>"
       value="<?php echo '' !== $value ? esc_attr( $value ) : esc_attr( $default ); ?>"
       required
>
</p>