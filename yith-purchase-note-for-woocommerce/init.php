<?php
/**
 * Plugin Name: YITH Purchase Note for WooCommerce
 * Description: Purchase Note for WooCommerce for YITH Plugins
 * Version: 1.0.0
 * Author: Juan Coronel Mendez
 * Author URI: https://yithemes.com/
 * Text Domain: yith-purchase-note-for-woocommerce
 *
 * @package .
 */

! defined( 'ABSPATH' ) && exit;   // Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_PNFW_VERSION' ) ) {
	define( 'YITH_PNFW_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PNFW_DIR_URL' ) ) {
	define( 'YITH_PNFW_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PNFW_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PNFW_DIR_ASSETS_URL', YITH_PNFW_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PNFW_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PNFW_DIR_ASSETS_CSS_URL', YITH_PNFW_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PNFW_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PNFW_DIR_ASSETS_JS_URL', YITH_PNFW_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PNFW_DIR_PATH' ) ) {
	define( 'YITH_PNFW_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PNFW_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PNFW_DIR_INCLUDES_PATH', YITH_PNFW_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PNFW_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PNFW_DIR_TEMPLATES_PATH', YITH_PNFW_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PNFW_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PNFW_DIR_VIEWS_PATH', YITH_PNFW_DIR_PATH . 'views' );
}

if ( ! defined( 'HOUR_IN_SECONDS' ) ) {
	define( 'HOUR_IN_SECONDS', 3600 );
}

// Different way to declare a constant.

! defined( 'YITH_PNFW_INIT' ) && define( 'YITH_PNFW_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PNFW_SLUG' ) && define( 'YITH_PNFW_SLUG', 'yith-purchase-note-for-woocommerce' );
! defined( 'YITH_PNFW_SECRETKEY' ) && define( 'YITH_PNFW_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );
! defined( 'YITH_PNFW_OPTIONS_PATH' ) && define( 'YITH_PNFW_OPTIONS_PATH', YITH_PNFW_DIR_PATH . 'plugin-options' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_pnfw_init_classes' ) ) {
	/**
	 * Yith_pnfw_init_classes
	 *
	 * @return void
	 */
	function yith_pnfw_init_classes() {

		load_plugin_textdomain( 'yith-purchase-note-for-woocommerce', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PNFW_DIR_INCLUDES_PATH . '/class-yith-pnfw-purchase-note.php';

		if ( class_exists( 'YITH_PNFW_Purchase_Note' ) ) {
			/*
			*	Call the main function
			*/
			yith_pnfw_purchase_note();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pnfw_init_classes', 11 );
