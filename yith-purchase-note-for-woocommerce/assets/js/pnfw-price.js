jQuery( function() {
   
    jQuery("input[name='_yith_pnfw_content_note'], textarea[name='_yith_pnfw_content_note']").on('keyup change', function() {
        var string = jQuery(this).val().length;
        //console.log(string);
        
       jQuery.ajax({
          type: 'post',          
          url: wp_ajax_tets_vars.ajax_url,
          data : {
            id: jQuery(this).attr('data-id'),
            action: 'increase_note_price', 
            message: string,
        },
          error: function (response){
              console.log(response['message']);
          },
          success: function(response) {
                console.log(response['message']);
                if(response['chars'] > -1) {
                    jQuery(".ppc").text(response['chars'])
                }
                jQuery(".ppp").text(response['message'])
            }
            
        });
    });
});