<?php
/**
 * This file belongs to the YITH PNFW Purchase Note for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PNFW_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PNFW_Ajax' ) ) {
	/**
	 * YITH_PNFW_Ajax
	 */
	class YITH_PNFW_Ajax {
		/**
		 * Main Instance
		 *
		 * @var YITH_PNFW_Ajax
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PNFW_Ajax Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PNFW_Ajax constructor.
		 */
		private function __construct() {

			add_action( 'wp_ajax_increase_note_price', array( $this, 'increase_note_price' ) );
			add_action( 'wp_ajax_nopriv_increase_note_price', array( $this, 'increase_note_price' ) );

		}
		/**
		 * Increase Note Price
		 *
		 * @return void
		 */
		public function increase_note_price() {
			$string      = isset( $_POST['message'] ) ? $_POST['message'] : '';
			$id          = isset( $_POST['id'] ) ? $_POST['id'] : '';
			$product     = wc_get_product( $id );
			$price       = $product->get_price();
			$final_price = 0;
			$updated     = true;

			if ( null !== $product->get_meta( '_yith_pnfw_price_settings' ) && ! empty( $product->get_meta( '_yith_pnfw_price_settings' ) ) ) {

				if ( 'free' === $product->get_meta( '_yith_pnfw_price_settings' ) ) {
					return;
				}

				if ( 'fixed_price' === $product->get_meta( '_yith_pnfw_price_settings' ) ) {
					$fixed_price = $product->get_meta( '_yith_pnfw_price' );
					$free_chars  = $product->get_meta( '_yith_pnfw_free_chars' );
					$sum         = $string - $free_chars;
					if ( $sum <= 0 ) {
						$final_price = $price;
					} else {
						$final_price = $price + $fixed_price;
					}
				}

				if ( 'price_per_char' === $product->get_meta( '_yith_pnfw_price_settings' ) ) {
					$price_per_char = $product->get_meta( '_yith_pnfw_price' );
					$free_chars     = $product->get_meta( '_yith_pnfw_free_chars' );
					$sum            = $string - $free_chars;
					if ( $sum <= 0 ) {
						$final_price = $price;
					} else {
						$final_price = $price + ( $sum * $price_per_char );
					}
				}

				$free_chars_now = -1;
				if ( isset( $free_chars ) ) {
					$min = $free_chars - $string;
					if ( $min >= 0 ) {
						$free_chars_now = $min;
					}
				}

				if ( $updated ) {
					wp_send_json(
						array(
							'message' => $final_price,
							'chars'   => $free_chars_now,
						)
					);
				} else {
					wp_send_json( array( 'message' => __( 'Error', 'yith-purchase-note-for-woocommerce' ) ) );
				}
			}

		}
	}
}
