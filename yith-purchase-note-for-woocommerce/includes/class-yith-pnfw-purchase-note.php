<?php
/**
 * This file belongs to the YITH PNFW Purchase Note for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PNFW_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PNFW_Purchase_Note' ) ) {
	/**
	 * YITH_PNFW_Purchase_Note
	 */
	class YITH_PNFW_Purchase_Note {

		/**
		 * Main Instance
		 *
		 * @var YITH_PNFW_Purchase_Note
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main Admin Instance
		 *
		 * @var YITH_PNFW_Purchase_Note_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PNFW_Purchase_Note_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main Shortcodes Instance
		 *
		 * @var YITH_PNFW_Purchase_Note_Frontend
		 *
		 * @since 1.0
		 */
		public $shortcodes = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PNFW_Purchase_Note Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6 .
		}
		/**
		 * YITH_PNFW_Purchase_Note constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_pnfw_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-yith-pnfw-ajax.php',
					),
					'admin'    => array(
						'includes/class-yith-pnfw-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pnfw-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			/*
				Here set any other hooks ( actions or filters you'll use on this class)
			*/
			// Finally call the init function.
			$this->init();

		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param array? $main_classes array The require classes file path.
		 *
		 * @author Juan Coronel
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_PNFW_DIR_PATH . $class ) ) {
						require_once( YITH_PNFW_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Juan Coronel
		 *
		 * @since  1.0
		 **/
		public function init_classes() {
			$this->ajax = YITH_PNFW_Ajax::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Juan Coronel Mendez
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_PNFW_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_PNFW_Frontend::get_instance();
			}
		}

	}
}
/**
 * Get the YITH_PNFW_Purchase_Note instance
 *
 * @return YITH_PNFW_Purchase_Note
 */
if ( ! function_exists( 'yith_pnfw_purchase_note' ) ) {
	/**
	 * Yith_pnfw_purchase_note
	 *
	 * @return Object?
	 */
	function yith_pnfw_purchase_note() {
		return YITH_PNFW_Purchase_Note::get_instance();
	}
}
