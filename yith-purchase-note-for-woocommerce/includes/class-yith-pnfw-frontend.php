<?php
/**
 * This file belongs to the YITH PNFW Purchase Note for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PNFW_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PNFW_Frontend' ) ) {
	/**
	 * YITH_PNFW_Frontend
	 */
	class YITH_PNFW_Frontend {
		/**
		 * Main Instance
		 *
		 * @var YITH_PNFW_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PNFW_Frontend Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PNFW_Frontend constructor.
		 */
		private function __construct() {

			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
			//add_action( 'wp_enqueue_scripts', array( $this, 'pnfw_custom_styles' ) );
			add_filter( 'woocommerce_product_thumbnails', array( $this, 'pnfw_show_badge_note_product' ) );
			add_action( 'woocommerce_before_shop_loop_item', array( $this, 'pnfw_show_badge_note_shop' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'pnfw_product_add_on' ), 9 );
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'pnfw_product_add_on_cart_item_data' ), 10, 2 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'pnfw_product_add_on_display_cart' ), 10, 2 );
			add_action( 'woocommerce_add_order_item_meta', array( $this, 'pnfw_product_add_on_order_item_meta' ), 10, 2 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'pnfw_custom_cart_item_price' ), 20, 1 );
			add_filter( 'woocommerce_order_item_product', array( $this, 'pnfw_product_add_on_display_order' ), 10, 2 );
			add_action( 'wp_enqueue_scripts', array( $this, 'pnfw_custom_styles' ) );

		}
		/**
		 * Enqueue_scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {

			wp_register_style( 'yith-pnfw-frontend-note-css', YITH_PNFW_DIR_ASSETS_CSS_URL . '/pnfw-frontend-note.css', array(), YITH_PNFW_VERSION );
			//wp_enqueue_style( 'yith-pnfw-frontend-note-css' );
			wp_register_script( 'pnfw-price-js', YITH_PNFW_DIR_ASSETS_JS_URL . '/pnfw-price.js', array( 'jquery' ), YITH_PNFW_VERSION ); //phpcs:ignore
			//wp_enqueue_script( 'pnfw-price-js' );
			wp_localize_script(
				'pnfw-price-js',
				'wp_ajax_tets_vars',
				array(
					'ajax_url' => admin_url( 'admin-ajax.php' ),
				)
			);
			if ( is_product() || is_shop() ) {
				wp_enqueue_style( 'yith-pnfw-frontend-note-css' );
				wp_enqueue_script( 'pnfw-price-js' );
				wp_enqueue_style( array( $this, 'pnfw_custom_styles' ) );
			}

		}
		/**
		 * Pnfw_custom_styles
		 *
		 * @return void
		 */
		public function pnfw_custom_styles() {
			global $post;
			$product        = wc_get_product( $post->ID );
			$padding_top    = get_option( 'yith_pnfw_padding_top', 20 ) . 'px';
			$padding_right  = get_option( 'yith_pnfw_padding_right', 25 ) . 'px';
			$padding_bottom = get_option( 'yith_pnfw_padding_bottom', 25 ) . 'px';
			$padding_left   = get_option( 'yith_pnfw_padding_left', 25 ) . 'px';
			$border_width   = get_option( 'yith_pnfw_border_width', 1 ) . 'px';
			$border_style   = get_option( 'yith_pnfw_border_select', 'solid' );
			$border_color   = get_option( 'yith_pnfw_border_color', 'd8d8d8' );
			$border_radius  = get_option( 'yith_pnfw_border_radius', '7' ) . 'px';
			/*null !== get_option( 'yith_pnfw_border_radius' ) && ! empty( get_option( 'yith_pnfw_border_radius' ) ) ? get_option( 'yith_pnfw_border_radius' ) . 'px' : 7 . 'px';*/

			$badge_tc = is_object( $product ) ? $product->get_meta( '_yith_pnfw_badge_tc' ) : '#ffffff';
			$badge_bc = is_object( $product ) ? $product->get_meta( '_yith_pnfw_badge_bc' ) : '#007694';

			$position_product = get_option( 'yith_pnfw_badge_position_product', 'top-right' );
			$position_shop    = get_option( 'yith_pnfw_badge_position_shop', 'top-right' );

			if ( 'top-right' === $position_shop ) {
				$position_shop = 'right';
			} elseif ( 'top-left' === $position_shop ) {
				$position_shop = 'left';
			}

			$custom_css = "
			.container-note{
					padding-top: {$padding_top};
					padding-right: {$padding_right};
					padding-bottom: {$padding_bottom};
					padding-left: {$padding_left};
					border-width: {$border_width};
					border-style: {$border_style};
					border-color: {$border_color};
					border-radius: {$border_radius};
			}
			.badge-note-product, .badge-note-shop{
				color: {$badge_tc};
				background-color: {$badge_bc};
			}
			.badge-note-shop{
					float: {$position_shop};
			}";

			if ( 'top-right' === $position_product ) {
				$custom_css .= "
				.badge-note-product{
						right: 0;
				}";
			} elseif ( 'top-left' === $position_product ) {
				$custom_css .= "
				.badge-note-product{
						left: 0;
				}";
			}

			wp_add_inline_style( 'yith-pnfw-frontend-note-css', $custom_css );
		}
		/**
		 * Make and show the html Note
		 *
		 * @return void
		 */
		public function pnfw_product_add_on() {
			global $post;
			$product = wc_get_product( $post->ID );

			if ( 'yes' === $product->get_meta( '_yith_pnfw_enable' ) ) {
				$value      = isset( $_POST['_yith_pnfw_content_note'] ) ? sanitize_text_field( wp_unslash( $_POST['_yith_pnfw_content_note'] ) ) : '';
				$free_chars = $product->get_meta( '_yith_pnfw_free_chars' );
				$data_note  = array(
					'class'             => 'container-note',
					'currency_symbol'   => get_woocommerce_currency_symbol(),
					'title_note'        => $product->get_meta( '_yith_pnfw_label' ),
					'title_description' => $product->get_meta( '_yith_pnfw_description' ),
					'value'             => $value,
					'id_post'           => $post->ID,
					'price_type'        => $product->get_meta( '_yith_pnfw_price_settings' ),
					'price_note'        => $product->get_meta( '_yith_pnfw_price' ),
					'price_product'     => $product->get_price(),
					'free_chars'        => $free_chars,
					'display_none'      => '0' === $free_chars ? 'display-none' : '',
				);
				$input_type = $product->get_meta( '_yith_pnfw_field_type' );
				yith_pnfw_get_view( '/input-fields' . '/' . $input_type . '.php', $data_note ); //phpcs:ignore				
			}

		}
		/**
		 * Pnfw_product_add_on_cart_item_data
		 *
		 * @param  mixed $cart_item
		 * @param  mixed $product_id
		 * @return void
		 */
		public function pnfw_product_add_on_cart_item_data( $cart_item, $product_id ) {
			if ( isset( $_POST['_yith_pnfw_content_note'] ) ) {
				$string      = strlen( sanitize_text_field( wp_unslash( $_POST['_yith_pnfw_content_note'] ) ) );
				$product     = wc_get_product( $product_id );
				$price       = $product->get_price();
				$price_type  = $product->get_meta( '_yith_pnfw_price_settings' );
				$note_price  = $product->get_meta( '_yith_pnfw_price' );
				$free_chars  = $product->get_meta( '_yith_pnfw_free_chars' );
				$final_price = 0;
				if ( 'free' === $price_type ) {
					$final_price = $price;
				}
				if ( 'fixed_price' === $price_type ) {
					$sum = $string - $free_chars;
					if ( $sum <= 0 ) {
						$final_price = $price;
					} else {
						$final_price = $price + $note_price;
					}
				}
				if ( 'price_per_char' === $price_type ) {
					$sum = $string - $free_chars;
					if ( $sum <= 0 ) {
						$final_price = $price;
					} else {
						$final_price = $price + ( $sum * $note_price );
					}
				}
				$cart_item['_yith_pnfw_content_note'] = sanitize_text_field( wp_unslash( $_POST['_yith_pnfw_content_note'] ) );
				$cart_item['title']                   = $product->get_meta( '_yith_pnfw_label' );
				$cart_item['final_price']             = $final_price;
			}
			return $cart_item;
		}
		/**
		 * Pnfw_product_add_on_display_cart
		 *
		 * @param  mixed $data
		 * @param  mixed $cart_item
		 * @return void
		 */
		public function pnfw_product_add_on_display_cart( $data, $cart_item ) {
			if ( isset( $cart_item['_yith_pnfw_content_note'] ) ) {
				$data[] = array(
					'name'  => sanitize_text_field( $cart_item['title'] ),
					'value' => sanitize_text_field( $cart_item['_yith_pnfw_content_note'] ),
				);
			}
			return $data;
		}
		/**
		 * Pnfw_product_add_on_order_item_meta
		 *
		 * @param  mixed $item_id
		 * @param  mixed $values
		 * @return void
		 */
		public function pnfw_product_add_on_order_item_meta( $item_id, $values ) {
			if ( ! empty( $values['_yith_pnfw_content_note'] ) ) {
				wc_add_order_item_meta( $item_id, $values['title'], $values['_yith_pnfw_content_note'], true );
			}
		}
		/**
		 * Pnfw_product_add_on_display_order
		 *
		 * @param  mixed $cart_item
		 * @param  mixed $order_item
		 * @return void
		 */
		public function pnfw_product_add_on_display_order( $cart_item, $order_item ) {
			if ( isset( $order_item['_yith_pnfw_content_note'] ) ) {
				$cart_item['_yith_pnfw_content_note'] = $order_item['_yith_pnfw_content_note'];
			}
			return $cart_item;
		}
		/**
		 * Pnfw_show_badge_note
		 *
		 * @return void
		 */
		public function pnfw_show_badge_note_product() {
			global $post;
			$product = wc_get_product( $post->ID );
			if ( 'yes' === $product->get_meta( '_yith_pnfw_show_badge' ) ) {
				$badge_text = null !== $product->get_meta( '_yith_pnfw_badge_text' ) ? $product->get_meta( '_yith_pnfw_badge_text' ) : '';
				echo '<p class="badge-note-product">' . esc_html( $badge_text ) . '</p>';
			}
		}

		/***
		 * Pnfw_show_badge_note_shop
		 *
		 * @return void
		 */
		public function pnfw_show_badge_note_shop() {
			global $post;
			$product = wc_get_product( $post->ID );
			if ( 'yes' === $product->get_meta( '_yith_pnfw_show_badge' ) ) {
				$badge_text = null !== $product->get_meta( '_yith_pnfw_badge_text' ) ? $product->get_meta( '_yith_pnfw_badge_text' ) : '';
				echo '<p class="badge-note-shop">' . esc_html( $badge_text ) . '</p>';
			}
		}
		/**
		 * Pnfw_custom_cart_item_price
		 *
		 * @param  mixed $cart
		 * @return void
		 */
		public function pnfw_custom_cart_item_price( $cart ) {
			if ( is_admin() && ! defined( 'DOING_AJAX' ) )
				return;

			if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
				return;

			foreach ( $cart->get_cart() as $cart_item ) {
				if ( null !== $cart_item['final_price'] ) {
					$cart_item['data']->set_price( $cart_item['final_price'] );
				}
			}
		}
	}
}
